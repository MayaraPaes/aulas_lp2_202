 <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <!-- Container wrapper -->
  <div class="container-fluid">
    <!-- Navbar brand -->
    <a class="navbar-brand" href="#">Controle Financeiro</a>

      <!-- Left links -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= base_url('home')?>">Home</a>
        </li>
      
        <!-- Navbar dropdown -->
             <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Cadastro
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a></li>
            <li><a class="dropdown-item" href="#">Conta Bancária</a></li>
            <li><a class="dropdown-item" href="#">Parceiros</a></li>
          </ul>
        </li>
        <li>
             <!-- Navbar dropdown -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Lançamentos
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Contas a Pagar</a></li>
            <li><a class="dropdown-item" href="#">Contas a Receber</a></li>
            <li><a class="dropdown-item" href="#">Fluxo de Caixa</a></li>
          </ul>
        </li>
        <li>
             <!-- Navbar dropdown -->
             <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
             Relatórios
          </a>
          <!-- Dropdown menu -->
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Lançamentos por Período</a></li>
            <li><a class="dropdown-item" href="#">Resumo Mensal</a></li>
            <li><a class="dropdown-item" href="#">Resumo Anual</a></li>
          </ul>
        </li>
        <li>
  

 
